using System;
using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [Serializable]
    public class LevelData
    {
        public int PlatformsCount;
        public int PointsForLevelComplete;
        public int PointsForPerfectComplete;
        public float[] RightTileScaleXRange;
        public float[] RightTileScaleZRange;
        public float[] LeftTileScaleXRange;
        public float[] LeftTileScaleZRange;
        public int PlatformsColorChangeCount;
        public Gradient PlatformsColor;
    }
}
