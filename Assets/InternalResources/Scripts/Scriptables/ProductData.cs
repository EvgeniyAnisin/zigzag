using System;
using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [Serializable]
    public class ProductData
    {
        public string name;
        public int type;
        public string id;
        public int coinsAmount;
        public string price;
        public string description;
        public string titleText;
        public Sprite coinSprite;
    }
}
