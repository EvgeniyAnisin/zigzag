using System;
using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [Serializable]
    public class BoxData
    {
        public int ballCost;
        public Color ballColor;
        public Material ballMaterial;
    }
}
