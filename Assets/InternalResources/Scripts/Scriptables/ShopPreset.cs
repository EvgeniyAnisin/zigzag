using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "ShopPreset", menuName = "ShopData")]
    public class ShopPreset : ScriptableObject
    {
        [SerializeField] private BoxData[] boxData;

        public BoxData[] BoxData => boxData;
    }
}
