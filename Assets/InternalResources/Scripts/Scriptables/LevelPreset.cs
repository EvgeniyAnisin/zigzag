using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [CreateAssetMenu(menuName = "LevelPreset", fileName = "LevelSettings")]
    public class LevelPreset : ScriptableObject
    {
        [SerializeField] private LevelData[] levelData;
        
        public LevelData[] LevelData => levelData;
    }
}
