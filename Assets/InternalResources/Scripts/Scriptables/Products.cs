using UnityEngine;

namespace InternalResources.Scripts.Scriptables
{
    [CreateAssetMenu(fileName = "IAPProducts", menuName = "ProductsData")]
    public class Products : ScriptableObject
    {
        [SerializeField] private ProductData[] productData;

        public ProductData[] ProductData => productData;
    }
}
