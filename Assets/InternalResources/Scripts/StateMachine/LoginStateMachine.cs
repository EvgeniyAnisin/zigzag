﻿using Core.Scripts.StateMachine;
using InternalResources.Scripts.LoginViewControllers.View;
using InternalResources.Scripts.PopUpViewControllers;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.ViewControllers;
using UnityEngine;

namespace DefaultNamespace
{
    public class LoginStateMachine : StateMachine<LoginStates>
    {
        public LoginStateMachine(LoginStates startState, Transform viewsParent) : base(startState, viewsParent)
        {
        }

        // protected override void SetupParams()
        // {
        //     viewsParams.Add(MainStates.MainMenu, new StateParams(true));
        // }
    }
}