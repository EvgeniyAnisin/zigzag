﻿using Core.Scripts.StateMachine;
using InternalResources.Scripts.PopUpViewControllers;
using InternalResources.Scripts.PopUpViewControllers.View;
using UnityEngine;

namespace DefaultNamespace
{
    public class PopUpStateMachine : StateMachine<PopUpStates>
    {
        public PopUpStateMachine(PopUpStates startState, Transform viewsParent) : base(startState, viewsParent)
        {
        }

        // protected override void SetupParams()
        // {
        //     viewsParams.Add(MainStates.MainMenu, new StateParams(true));
        // }
    }
}