﻿using Core.Scripts.StateMachine;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;

namespace DefaultNamespace
{
    public class MainStateMachine : StateMachine<MainStates>
    {
        public MainStateMachine(MainStates startState, Transform viewsParent) : base(startState, viewsParent)
        {
        }

        // protected override void SetupParams()
        // {
        //     viewsParams.Add(MainStates.MainMenu, new StateParams(true));
        // }
    }
}