using System;
using System.Linq;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.LoginViewControllers.View;
using InternalResources.Scripts.ViewControllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.LoginViewControllers
{
    public class RegistrationMenuViewController : ViewController<LoginStates>
    {
        public override LoginStates ViewState => LoginStates.RegistrationMenu;
        [Inject] private IStateMachine<LoginStates> _loginStateMachine;
        [Inject] private FirebaseManager _firebaseManager;
        [SerializeField] private TMP_InputField playerName;
        [SerializeField] private TMP_InputField playerEmail;
        [SerializeField] private TMP_InputField playerPassword;
        [SerializeField] private Button create;
        [SerializeField] private Button loginMenu;
        [SerializeField] private TextMeshProUGUI errorText;

        public override void Initialize()
        {
            create.onClick.AddListener(CreateButtonClick);
            loginMenu.onClick.AddListener(LoginButtonClick);
            _firebaseManager.DisplayRegistrationErrorMessage += DisplayErrorMessage;
        }

        private void OnEnable()
        {
            playerName.text = String.Empty;
            playerEmail.text = String.Empty;
            playerPassword.text = String.Empty;
        }

        private void CreateButtonClick()
        {
            if (!playerName.text.Any(char.IsLetter))
            {
                errorText.text = "Incorrect name";
                return;
            }
            _firebaseManager.CreateNewUser(playerEmail.text , playerPassword.text, playerName.text);
        }

        private void LoginButtonClick()
        {
            _loginStateMachine.Fire(LoginStates.LoginMenu);
        }

        private void DisplayErrorMessage(string message)
        {
            errorText.text = message;
        }

        private void OnDestroy()
        {
            _firebaseManager.DisplayRegistrationErrorMessage -= DisplayErrorMessage;
        }
    }
}
