using Core.Scripts.StateMachine;
using InternalResources.Scripts.LoginViewControllers.View;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace InternalResources.Scripts.LoginViewControllers.Controller
{
    public class LoginMenuController : MonoBehaviour
    {
        [Inject] private IStateMachine<LoginStates> _loginStateMachine;

        public void ChangeLoginMenuState(bool state)
        {
            Debug.Log(state);
            SceneManager.LoadScene(state ? 1 : 0);
        }
        
        public void RegistrationButtonClick()
        {
            _loginStateMachine.Fire(LoginStates.RegistrationMenu);
        }
    }
}
