using System;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.LoginViewControllers.Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.LoginViewControllers.View
{
    public class LoginMenuViewController : ViewController<LoginStates>
    {
        public override LoginStates ViewState => LoginStates.LoginMenu;

        [Header("Controller")] 
        [SerializeField] private LoginMenuController loginMenuController;
        [Inject] private FirebaseManager _firebaseManager;
        [SerializeField] private TMP_InputField playerEmail;
        [SerializeField] private TMP_InputField playerPassword;
        [SerializeField] private Button applyButton;
        [SerializeField] private Button registrationButton;
        [SerializeField] private TextMeshProUGUI errorText;

        public override void Initialize()
        {
            applyButton.onClick.AddListener(ApplyButtonClick);
            registrationButton.onClick.AddListener(loginMenuController.RegistrationButtonClick);
            _firebaseManager.LoginScreenState += loginMenuController.ChangeLoginMenuState;
            _firebaseManager.DisplayLoginErrorMessage += DisplayErrorMessage;
            if(_firebaseManager.IsUserLoggedIn) loginMenuController.ChangeLoginMenuState(true);
        }

        private void OnEnable()
        {
            playerEmail.text = String.Empty;
            playerPassword.text = String.Empty;
        }
        
        private void ApplyButtonClick()
        {
            _firebaseManager.LoginUser(playerEmail.text, playerPassword.text);
        }

        private void DisplayErrorMessage(string message)
        {
            errorText.text = message;
        }

        private void OnDestroy()
        {
            _firebaseManager.DisplayLoginErrorMessage -= DisplayErrorMessage;
        }
    }

    public enum LoginStates
    {
        LoginMenu,
        RegistrationMenu
    }
}
