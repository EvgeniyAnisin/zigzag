using System.Collections;
using UnityEngine;

namespace InternalResources.Scripts
{
    public class ParticleObject : MonoBehaviour
    {
        private void OnEnable()
        {
            StartCoroutine(DisableDelay());
        }

        private IEnumerator DisableDelay()
        {
            yield return new WaitForSeconds(2);
            gameObject.SetActive(false);
        }
    }
}
