using UnityEngine;

namespace InternalResources.Scripts
{
    public class EndTile : Tile
    {
        private void Awake()
        {
            _renderer = transform.GetChild(0).GetComponent<Renderer>();
            _defaultColor = _renderer.material.color;
        }
    
        private void OnDisable()
        {
            _renderer.material.SetColor("_Color", _defaultColor);
        }

        public override void ReturnToStartPosition()
        {
            return;
        }
    }
}
