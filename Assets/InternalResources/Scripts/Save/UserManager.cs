using System;
using System.Collections.Generic;
using InternalResources.Scripts.Firebase;
using Zenject;

namespace InternalResources.Scripts.Save
{
    public class UserManager : IInitializable, IDisposable
    {
        [Inject] private FirebaseManager _firebaseManager;
        private User _user;

        public void Initialize()
        {
            _firebaseManager.OnUserReceived += SetUser;
        }
        
        public void Dispose()
        {
            _firebaseManager.OnUserReceived -= SetUser;
        }

        private void SetUser(User user) => _user = user;

        public void SetGems(int amount) => _user.playerData.playerGems = amount;

        public int GetGems() => _user.playerData.playerGems;

        public void AddGems(int amount) => _user.playerData.playerGems += amount;
        
        public void ReduceGems(int amount) => _user.playerData.playerGems -= amount;

        public void SetLevel(int level) => _user.playerData.currentLevel = level;
        
        public int GetLevel() => _user.playerData.currentLevel;

        public void SetScore(int score) => _user.playerData.playerScore = score;

        public void AddScore(int score) => _user.playerData.playerScore += score;

        public int GetScore() => _user.playerData.playerScore;

        public void SaveUserData() => _firebaseManager.SavePlayerData(_user.playerData);

        public void SaveUserSaves() => _firebaseManager.SavePlayerSaves(_user.playerSaves);

        public void SetUserName(string playerName) => _user.playerName = playerName;

        public void AddBox(int boxId) => _user.playerSaves.collectedBoxes.Add(boxId);
        
        public List<int> GetBoxesList() => _user.playerSaves.collectedBoxes;
        
        public void SetSelectedBox(int boxId) => _user.playerSaves.selectedBox = boxId;

        public int GetSelectedBox() => _user.playerSaves.selectedBox;

        public PlayerData GetUser() => _user.playerData;
    }
}
