using System.IO;
using InternalResources.Scripts;
using UnityEngine;
using File = System.IO.File;

namespace Save
{
    public static class SaveSystem
    {
        private static readonly string SaveFolder = Application.dataPath + "/Saves/";

        public static void Init()
        {
            if (!Directory.Exists(SaveFolder))
            {
                Directory.CreateDirectory(SaveFolder);
            }
        }
        
        public static void Save(TileSpawner tileSpawner, GemsSpawner gemsSpawner)
        {
            LevelSaveData levelSaveData = new LevelSaveData(tileSpawner, gemsSpawner);
            string json = JsonUtility.ToJson(levelSaveData);
            File.WriteAllText(SaveFolder + "save.txt", json);
        }

        public static LevelSaveData Load()
        {
            if (File.Exists(SaveFolder + "save.txt"))
            {
                string saveString = File.ReadAllText(SaveFolder + "save.txt");
                return JsonUtility.FromJson<LevelSaveData>(saveString);
            }

            return null;
        }
    }
}
