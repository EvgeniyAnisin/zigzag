using System.Collections.Generic;

namespace InternalResources.Scripts.Save
{
    [System.Serializable]
    public class PlayerData
    {
        public int currentLevel;
        public int playerScore;
        public int playerGems;

        public PlayerData(int level, int score, int gems)
        {
            currentLevel = level;
            playerScore = score;
            playerGems = gems;
        }
    }

    [System.Serializable]
    public class PlayerSaves
    {
        public List<int> collectedBoxes = new List<int>();
        public int selectedBox;

        public PlayerSaves()
        {
            collectedBoxes.Add(0);
            selectedBox = 0;
        }
    }

    public class User
    {
        public string playerName;
        public bool playerNoAds;
        public PlayerData playerData;
        public PlayerSaves playerSaves;
        
        public User(string playerName, bool playerNoAds, PlayerData playerData, PlayerSaves playerSaves)
        {
            this.playerName = playerName;
            this.playerNoAds = playerNoAds;
            this.playerData = playerData;
            this.playerSaves = playerSaves;
        }
    }
}
