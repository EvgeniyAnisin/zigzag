using InternalResources.Scripts;
using UnityEngine;

namespace Save
{
    [System.Serializable]
    public class LevelSaveData
    {
        public string[] platformsNames;
        //public float platformsYPosition;
        //public float[] platformsPositions;
        //public float[] platformsScales;
        //public float[] gemsPositions;
        //public float gemsYPosition;

        public Vector3[] platformsPositions;
        public Vector3[] platformsScales;
        public Vector3[] gemsPositions;


        public LevelSaveData(TileSpawner tileSpawner, GemsSpawner gemsSpawner)
        {
            var platforms = tileSpawner.TilesInGameParent;
            var platformsCount = platforms.childCount;
            platformsNames = new string[platformsCount];
            platformsPositions = new Vector3[platformsCount];
            platformsScales = new Vector3[platformsCount];
            for (int i = 0; i < platformsCount; i++)
            {
                var platform = platforms.GetChild(i);
                platformsNames[i] = platform.name;
                platformsPositions[i] = platform.position;
                platformsScales[i] = platform.localScale;
            }
            var gems = gemsSpawner.GemsInGamePool;
            gemsPositions = new Vector3[gems.childCount];
            for (int i = 0; i < gems.childCount; i++)
            {
                gemsPositions[i] = gems.GetChild(i).position;
            }
        }

        // public LevelSaveData(TileSpawner tileSpawner, GemsSpawner gemsSpawner)
        // {
        //     var platforms = tileSpawner.TilesInGameParent;
        //     platformsYPosition = platforms.GetChild(0).position.y;
        //     var platformsCount = platforms.childCount;
        //     platformsNames = new string[platformsCount];
        //     platformsPositions = new float[platformsCount * 2];
        //     platformsScales = new float[platformsCount * 2];
        //     for (int i = 0; i < platforms.childCount; i++)
        //     {
        //         var platform = platforms.GetChild(i);
        //         platformsNames[i] = platform.name;
        //         var platformPosition = platform.position;
        //         platformsPositions[i] = platformPosition.x;
        //         platformsPositions[i + 1] = platformPosition.z;
        //         var platformScale = platform.localScale;
        //         platformsScales[i] = platformScale.x;
        //         platformsScales[i + 1] = platformScale.z;
        //     }
        //
        //     var gems = gemsSpawner.GemsInGamePool;
        //     gemsYPosition = gems.GetChild(0).position.y;
        //     gemsPositions = new float[gems.childCount * 2];
        //     for (int i = 0; i < gems.childCount; i++)
        //     {
        //         var gem = gems.GetChild(i);
        //         var gemPosition = gem.position;
        //         gemsPositions[i] = gemPosition.x;
        //         gemsPositions[i + 1] = gemPosition.z;
        //     }
        // }
    }
}
