using System;
using System.Collections.Generic;
using Core.Scripts.CoreScripts.Observer;
using Zenject;

namespace InternalResources.Scripts
{
    public class GemsController : IDataObservable<GemsManager>, IInitializable
    {
        public GemsManager Data { get; protected set; }
        
        [Inject] private GemsManager _gemsManager;
        private readonly List<IObservableNotifier<GemsManager>> _observables = new List<IObservableNotifier<GemsManager>>();
    
        public IDisposable Subscribe(IObservableNotifier<GemsManager> observer)
        {
            if (!_observables.Contains(observer))
            {
                if (Data != null)
                {
                    observer.Notify(Data);
                }

                _observables.Add(observer);
            }
            return new Unsubscriber<GemsManager>(_observables, observer);
        }

        public void Initialize()
        {
            Data = _gemsManager;
        }

        public void AddGem(int amount = 1)
        {
            Data.AddGem(amount);
            
            _observables.ForEach(f=>f?.Notify(Data));
        }

        public bool ReduceGems(int amount)
        {
            if (!Data.ReduceGems(amount)) return false;
            _observables.ForEach(f => f?.Notify(Data));
            return true;
        }

        public void SetGems(int amount)
        {
            Data.SetGems(amount);
        
            _observables.ForEach(f=>f?.Notify(Data));
        }
    }
}
