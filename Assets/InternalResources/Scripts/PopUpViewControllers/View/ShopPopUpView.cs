using System;
using Core.Scripts.CoreScripts.Observer;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class ShopPopUpView : ViewController<PopUpStates>, IObservableNotifier<GemsManager>
    {
        public override PopUpStates ViewState => PopUpStates.Shop;

        [Header("Controller")] 
        [SerializeField] private ShopPopUpController shopPopUpController;
        
        [Inject] private IDataObservable<GemsManager> _observable;
        [Inject] private GemsManager _gemsManager;
        
        [SerializeField] private TextMeshProUGUI gemsText;
        [SerializeField] private Button backButton;

        [Header("BoxData")] 
        [SerializeField] private GameObject boxPrefab;
        [SerializeField] private Transform boxesParent;
        
        private IDisposable _disposable;

        private void OnEnable()
        {
            gemsText.text = _gemsManager.GemsCount().ToString();
        }

        public override void Initialize()
        {
            _disposable = _observable.Subscribe(this);
            backButton.onClick.AddListener(shopPopUpController.BackButtonClick);
            //shopPopUpController.SpawnShopBoxes(boxPrefab, boxesParent);
            shopPopUpController.Init(boxPrefab, boxesParent);
            //shopPopUpController.SetSelectedBox();
        }
        
        public void Notify(GemsManager data)
        {
            gemsText.text = data.GemsCount().ToString();
        }
        
        private void OnDestroy()
        {
            _disposable?.Dispose();
        }
    }
}
