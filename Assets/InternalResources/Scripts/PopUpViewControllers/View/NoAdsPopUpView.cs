using System;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using InternalResources.Scripts.Scriptables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class NoAdsPopUpView : ViewController<PopUpStates>
    {
        public override PopUpStates ViewState => PopUpStates.NoAds;
        [Inject] private Products _iapProducts;

        [Header("Controller")] [SerializeField]
        private NoAdsPopUpController noAdsPopUpController;
        
        
        [SerializeField] private TextMeshProUGUI priceText;
        [SerializeField] private GameObject noAdsButton;
        [SerializeField] private Button buyButton;
        [SerializeField] private Button closeButton;
        
        public override void Initialize()
        {
            closeButton.onClick.AddListener(noAdsPopUpController.CloseButtonClick);
            noAdsPopUpController.SubscribeBuyButton(buyButton);
            noAdsPopUpController.Init();
            noAdsPopUpController.DeactivateNoAdsButton += () => noAdsButton.SetActive(false);
            priceText.text = _iapProducts.ProductData[0].price;
        }

        private void OnDestroy()
        {
            noAdsPopUpController.DeactivateNoAdsButton -= () => noAdsButton.SetActive(false);
        }
    }
}
