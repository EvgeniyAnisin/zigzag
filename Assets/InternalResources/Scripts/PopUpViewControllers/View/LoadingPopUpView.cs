using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class LoadingPopUpView : ViewController<PopUpStates>
    {
        public override PopUpStates ViewState => PopUpStates.Loading;

        [Header("Controller")] 
        [SerializeField] private LoadingPopUpController loadingPopUpController;
        
        [Inject] private FirebaseManager _firebaseManager;


        public override void Initialize()
        {
            //_firebaseManager.LoadingState += LoadingComplete;
            loadingPopUpController.LoadPlayerData();
        }

        private void LoadingComplete()
        {
            //_popUpStateMachine.Fire(PopUpStates.None);
        }

        private void OnDestroy()
        {
            //_firebaseManager.LoadingState -= LoadingComplete;
        }
    }
}
