using Core.Scripts.StateMachine;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class EmptyPopUpView : ViewController<PopUpStates>
    {
        public override PopUpStates ViewState => PopUpStates.None;
    }
    
    public enum PopUpStates
    {
        None,
        Leaderboard,
        NoAds,
        Settings,
        Shop,
        Loading
    }
}
