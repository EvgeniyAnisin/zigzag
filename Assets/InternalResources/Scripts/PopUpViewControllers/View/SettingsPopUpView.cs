using System;
using System.Linq;
using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using InternalResources.Scripts.Save;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class SettingsPopUpView : ViewController<PopUpStates>
    {
        public override PopUpStates ViewState => PopUpStates.Settings;
        [Inject] private FirebaseManager _firebaseManager;
        [Inject] private UserManager _userManager;

        [Header("Controller")] 
        [SerializeField] private SettingsPopUpController settingsPopUpController;
        
        
        [SerializeField] private Button logout;
        [SerializeField] private Button backButton;
        [SerializeField] private TMP_InputField newPlayerName;
        [SerializeField] private Button setName;
        [SerializeField] private TextMeshProUGUI errorText;
        
        private void OnEnable()
        {
            errorText.text = String.Empty;
        }

        public override void Initialize()
        {
            logout.onClick.AddListener(settingsPopUpController.LogoutButtonClick);
            backButton.onClick.AddListener(settingsPopUpController.BackButtonClick);
            setName.onClick.AddListener(SetNameButtonClick);
        }

        private void SetNameButtonClick()
        {
            if (!newPlayerName.text.Any(char.IsLetter))
            {
                errorText.text = "Incorrect name";
                errorText.color = Color.red;
                return;
            }
            var playerName = newPlayerName.text.Trim();
            _userManager.SetUserName(playerName);
            _firebaseManager.SetPlayerName(playerName);
            errorText.text = "Name changed";
            errorText.color = Color.green;
        }
    }
}
