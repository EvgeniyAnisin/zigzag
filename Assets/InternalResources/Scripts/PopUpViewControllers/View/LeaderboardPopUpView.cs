using System;
using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.View
{
    public class LeaderboardPopUpView : ViewController<PopUpStates>
    {
        public override PopUpStates ViewState => PopUpStates.Leaderboard;
        [Inject] private FirebaseManager _firebaseManager;

        [Header("Controller")] [SerializeField]
        private LeaderBoardPopUpController leaderBoardPopUpController;

        [SerializeField] private Button closeButton;
        
        [Header("Leaderboard Links")] 
        [SerializeField] private GameObject playerBox;
        [SerializeField] private Transform playersContainer;

        private void OnEnable()
        {
            leaderBoardPopUpController.CreateLeaderboard(playerBox, playersContainer);
        }

        public override void Initialize()
        {
            closeButton.onClick.AddListener(leaderBoardPopUpController.CloseButtonClick);
        }

        private void DisablePlayerBoxes()
        {
            for (int i = 0; i < playersContainer.childCount; i++)
            {
                playersContainer.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void OnDisable()
        {
            DisablePlayerBoxes();
        }
    }
}
