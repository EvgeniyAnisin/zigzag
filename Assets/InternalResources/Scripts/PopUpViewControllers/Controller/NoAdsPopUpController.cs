using System;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.Ads;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.InAppPurchasing;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.Scriptables;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.Controller
{
    public class NoAdsPopUpController : MonoBehaviour
    {
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private Products _iapProducts;
        [Inject] private InAppManager _inAppManager;
        [Inject] private AdManager _adManager;
        [Inject] private FirebaseManager _firebaseManager;

        public Action DeactivateNoAdsButton;

        public void Init()
        {
            _inAppManager.PurchaseCompleted += PurchaseCompleteHandler;
        }

        public void SubscribeBuyButton(Button buyButton)
        {
            Subscribe(_inAppManager.Purchase, buyButton);
        }

        public void CloseButtonClick()
        {
           _popUpStateMachine.Fire(PopUpStates.None);
        }

        private void Subscribe(Action<string> onClick, Button buyButton)
        {
            buyButton.onClick.AddListener(() => onClick?.Invoke(_iapProducts.ProductData[0].id));
        }

        private void PurchaseCompleteHandler(ProductData data)
        {
            if (data.name != "NoAds") return;
            //PlayerPrefs.SetInt("NoAds", 1);
            _firebaseManager.SetNoAdsState(true);
            _adManager.IsNoAdsActive = true;
            DeactivateNoAdsButton?.Invoke();
            CloseButtonClick();
        }
        
        private void OnDestroy()
        {
            _inAppManager.PurchaseCompleted -= PurchaseCompleteHandler;
        }
    }
}
