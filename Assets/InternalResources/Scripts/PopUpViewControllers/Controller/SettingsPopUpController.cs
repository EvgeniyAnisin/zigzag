using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.PopUpViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.Controller
{
    public class SettingsPopUpController : MonoBehaviour
    {
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private TileSpawner _tileSpawner;
        [Inject] private FirebaseManager _firebaseManager;

        public void LogoutButtonClick()
        {
            _firebaseManager.LogoutUser();
            _tileSpawner.RestartLevel(false);
            _popUpStateMachine.Fire(PopUpStates.None);
        }

        public void BackButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.None);
        }
    }
}
