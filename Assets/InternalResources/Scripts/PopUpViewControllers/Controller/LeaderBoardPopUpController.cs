using System;
using System.Linq;
using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.PopUpViewControllers.View;
using UI;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.Controller
{
    public class LeaderBoardPopUpController : MonoBehaviour
    {
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private FirebaseManager _firebaseManager;

        public void CloseButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.None);
        }
        
        public async void CreateLeaderboard(GameObject playerBox, Transform playersContainer)
        {
            var user = _firebaseManager.GetUser();
            var leaders = await _firebaseManager.GetLeaders();
            int i = 1;
            foreach (var player in leaders.Children.Reverse())
            {
                var playerName = player.Child("playerName").Value.ToString();
                var playerScore = Int32.Parse(player.Child("playerData").Child("playerScore").Value.ToString());
                bool isCurrentPlayer = player.Key == user.UserId;
            
                var playerPanel = GetPlayerPanel(playerBox, playersContainer);
                playerPanel.GetComponent<PlayerPanel>().Init(i, playerName, playerScore, isCurrentPlayer);
                i++;
            }
        }
        
        private GameObject GetPlayerPanel(GameObject playerBox, Transform playersContainer)
        {
            GameObject playerPanel;
            for (int i = 0; i < playersContainer.childCount; i++)
            {
                playerPanel = playersContainer.GetChild(i).gameObject;
                if (!playerPanel.activeSelf)
                {
                    playerPanel.SetActive(true);
                    return playerPanel;
                }
            }
            playerPanel = Instantiate(playerBox, playersContainer.transform.position, Quaternion.identity,
                playersContainer);
            return playerPanel;
        }
    }
}
