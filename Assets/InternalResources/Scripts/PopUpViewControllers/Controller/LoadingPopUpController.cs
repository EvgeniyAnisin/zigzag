using System;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.Ads;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.Controller
{
    public class LoadingPopUpController : MonoBehaviour
    {
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private IStateMachine<MainStates> _stateMachine;
        
        [Inject] private FirebaseManager _firebaseManager;
        [Inject] private GemsController _gemsController;
        [Inject] private AdManager _adManager;
        [Inject] private TileSpawner _tileSpawner;
        [Inject] private PlayerObject _playerObject;

        [SerializeField] private ShopPopUpController shopPopUpController;
        [SerializeField] private MainMenuButtonsView mainMenuButtonsView;

        public async void LoadPlayerData()
        {
            var userData = await _firebaseManager.GetPlayerData();
            _gemsController.SetGems(userData.playerData.playerGems);
            _adManager.IsNoAdsActive = Convert.ToBoolean(userData.playerNoAds);
            mainMenuButtonsView.SetNoAdsButton(userData.playerNoAds);
            _playerObject.SetBallMaterial();
            shopPopUpController.SpawnShopBoxes();
            shopPopUpController.SetSelectedBox();
            _tileSpawner.SpawnTiles();
            _stateMachine.Fire(MainStates.MainMenu);
            _popUpStateMachine.Fire(PopUpStates.None);
        }
    }
}
