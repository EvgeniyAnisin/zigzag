using System;
using System.Collections.Generic;
using Core.Scripts.CoreScripts.Observer;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.Scriptables;
using InternalResources.Scripts.UI;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.PopUpViewControllers.Controller
{
    public class ShopPopUpController : MonoBehaviour
    {
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private ShopPreset _shopPreset;
        [Inject] private DiContainer _container;
        [Inject] private UserManager _userManager;

        private List<ShopBox> _shopBoxes = new List<ShopBox>();
        private GameObject _boxPrefab;
        private Transform _boxesParent;

        public void Init(GameObject boxPrefab, Transform boxesParent)
        {
            _boxPrefab = boxPrefab;
            _boxesParent = boxesParent;
        }

        public void BackButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.None);
        }

        public void SpawnShopBoxes()
        {
            for (int i = 0; i < _shopPreset.BoxData.Length; i++)
            {
                var boxObject = Instantiate(_boxPrefab, _boxesParent.position, Quaternion.identity, _boxesParent).GetComponent<ShopBox>();
                _container.Inject(boxObject);
                boxObject.Init(_shopPreset.BoxData[i], i, this);
                _shopBoxes.Add(boxObject);
            }
        }

        public void SetSelectedBox()
        {
            var selectedBoxId = _userManager.GetSelectedBox();
            for (int i = 0; i < _shopBoxes.Count; i++)
            {
                _shopBoxes[i].SetSelectedState(selectedBoxId);
            }
        }

        private void DestroyBoxes()
        {
            for (int i = _boxesParent.childCount - 1; i >= 0; i--)
            {
                Destroy(_boxesParent.GetChild(i).gameObject);
            }
        }

        private void OnDestroy()
        {
            DestroyBoxes();
        }
    }
}
