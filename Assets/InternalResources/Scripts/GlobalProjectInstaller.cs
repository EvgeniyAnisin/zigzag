using Firebase;
using InternalResources.Scripts.Ads;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.InAppPurchasing;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.Scriptables;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class GlobalProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<UserManager>().AsSingle();
            Container.Bind<Products>().FromScriptableObjectResource("IAPProducts").AsTransient();
            Container.BindInterfacesAndSelfTo<InAppManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<AdManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<FirebaseManager>().AsSingle();
        }
    }
}
