using DefaultNamespace;
using InternalResources.Scripts.LoginViewControllers.View;
using InternalResources.Scripts.ViewControllers;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class LoginSceneInstaller : MonoInstaller
    {
        [SerializeField] private Transform loginParent;
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<LoginStateMachine>()
                .AsSingle()
                .WithArguments(LoginStates.LoginMenu, loginParent)
                .NonLazy();
        }
    }
}
