using System.Collections;
using DG.Tweening;
using InternalResources.Scripts.Save;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace InternalResources.Scripts
{
    public class ScoreManager : MonoBehaviour, IPointerDownHandler
    {
        [Inject] private TileSpawner _tileSpawner;
        [Inject] private GemsManager _gemsManager;
        [Inject] private UserManager _userManager;
    
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI bonusText;
        [SerializeField] private float addingTime;
        [SerializeField] private Transform scoreTransform;
        [SerializeField] private Transform scoreStartTransform;
        [SerializeField] private Transform scoreEndTransform;
        [SerializeField] private float scoreTransitionTime;

        [Header("Buttons")] 
        [SerializeField] private GameObject resetButton;
        [SerializeField] private GameObject nextLevelButton;

        private int _gameScore;
        private int _finalScore;
        
        private int _scoreForPlatforms;

        private Tween _scoreAddTween;
        private Tween _scoreSizeTween;

        private Tween _scoreTween;

        private Sequence _scoreSequence;

        private bool _isSkipped;
        private State _state;

        private int _scoreValue;

        public int LevelScore => _finalScore;
        
        public enum State
        {
            Win,
            Lose
        }

        private void OnEnable()
        {
            ScoreAdding(_state);
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if(_isSkipped) return;
            //DOTween.Pause(_scoreTween);
            DOTween.KillAll();
            if(_state == State.Win) nextLevelButton.SetActive(true);
            else resetButton.SetActive(true);
            scoreText.text = _finalScore.ToString();
            _isSkipped = true;
        }

        public void CalculateWinScore(State state)
        {
            _gameScore = _userManager.GetScore();
            _state = state;
            var currentLevel = _userManager.GetLevel();
            var levelData = _tileSpawner.LevelPreset.LevelData;
            switch (state)
            {
                case State.Win:
                    _finalScore = (1000 * levelData[currentLevel].PointsForLevelComplete *
                            (currentLevel + 1) / 2) + _gemsManager.GetCollectedGemsCount() * 10 *
                        levelData[currentLevel].PointsForPerfectComplete;
                    levelText.text = "LEVEL COMPLETE!";
                    levelText.color = Color.green;
                    break;
                case State.Lose:
                    _finalScore = _gemsManager.GetCollectedGemsCount() * 10;
                    levelText.text = "LEVEL FAILED!";
                    levelText.color = Color.red;
                    break;
            }

            _scoreForPlatforms = Mathf.FloorToInt(_tileSpawner.TilesReached * 0.1f) * 200;
            _finalScore += _scoreForPlatforms;
            _gameScore += _state == State.Win ? _finalScore : 0;
            _userManager.SetScore(_gameScore);
            _gemsManager.ResetCollectedGems();
            Debug.Log(_finalScore);
            Debug.Log(_scoreForPlatforms);
        }

        private void ScoreAdding(State state)
        {
            scoreTransform.position = scoreStartTransform.position;
            nextLevelButton.SetActive(false);
            resetButton.SetActive(false);
            _scoreValue = 0;
            scoreText.text = _scoreValue.ToString();
            _scoreSequence = DOTween.Sequence();
            _scoreSequence.Append(DOVirtual.DelayedCall(0.3f, () =>
            {
                var textScale = scoreText.transform.localScale;
                DOTween.To(() => _scoreValue, x => _scoreValue = x, _finalScore - _scoreForPlatforms, addingTime)
                    .OnUpdate(() => SetScoreText(_scoreValue));
                scoreText.transform.DOScale(1.5f, addingTime).OnComplete(() =>
                {
                    scoreText.transform.localScale = textScale;
                    DOVirtual.DelayedCall(0.5f, () =>
                    {
                        bonusText.gameObject.SetActive(true);
                        bonusText.text = $"platforms bonus +{_scoreForPlatforms}";
                    }).OnComplete(() =>
                    {
                        DOVirtual.DelayedCall(0.5f, () =>
                        {
                            DOTween.To(() => _scoreValue, x => _scoreValue = x, _finalScore, addingTime)
                                .OnUpdate(() => SetScoreText(_scoreValue)).OnComplete(() =>
                                {
                                    bonusText.gameObject.SetActive(false);
                                    DOVirtual.DelayedCall(0.2f, () =>
                                    {
                                        scoreTransform.transform.DOMove(scoreEndTransform.transform.position,
                                            scoreTransitionTime).OnComplete(() =>
                                        {
                                            if (state == State.Win) nextLevelButton.SetActive(true);
                                            else resetButton.SetActive(true);
                                        });
                                    });
                                });
                        });
                    });
                });
            }));
        }

        private void SetScoreText(int score)
        {
            scoreText.text = score.ToString();
        }

        private void OnDisable()
        {
            _isSkipped = false;
        }
    }
}
