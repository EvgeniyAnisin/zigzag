using System.Collections;
using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class LevelComplete : MonoBehaviour
    {
        [Inject] private IStateMachine<MainStates> _stateMachine;
        [Inject] private TileSpawner _tileSpawner;
        [Inject] private ScoreManager _scoreManager;
        [Inject] private GemsManager _gemsManager;
        [Inject] private GemsController _gemsController;
        [Inject] private UserManager _userManager;
        [Inject] private PlayerObject _playerObject;
        private void OnTriggerEnter(Collider other)
        {
            PlayerMovement playerMovement;
            if (playerMovement = other.GetComponent<PlayerMovement>())
            {
                StartCoroutine(Delay(playerMovement));
            }
        }

        private IEnumerator Delay(PlayerMovement playerMovement)
        {
            yield return new WaitForSeconds(0.3f);
            playerMovement.StopBall();
            //_userManager.AddGems(_gemsManager.GetCollectedGemsCount());
            _gemsController.AddGem(_gemsManager.GetCollectedGemsCount());
            _playerObject.SetPlayState(false);
            _scoreManager.CalculateWinScore(ScoreManager.State.Win);
            _stateMachine.Fire(MainStates.ScoreMenu);
            var currentLevel = _userManager.GetLevel();
            if (_tileSpawner.LevelsCount - 1 <= currentLevel) currentLevel = 0;
            else currentLevel++;
            _userManager.SetLevel(currentLevel);
            _userManager.SaveUserData();
        }
    }
}
