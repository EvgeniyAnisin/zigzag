using UnityEngine;

namespace InternalResources.Scripts
{
    public class GemRotation : MonoBehaviour
    {

        [SerializeField] private Vector3 rotationAngle;
        [SerializeField] private float rotationSpeed;
        private Rigidbody _gemRigidbody;
        
        private void Start()
        {
            _gemRigidbody = GetComponent<Rigidbody>();
            //_gemRigidbody.DORotate(new Vector3(0, 10, 0), 1).SetLoops(-1);
            //transform.DORotate(new Vector3(0, 10, 0), 1).SetLoops(-1);
        }

        private void Update()
        {
            Quaternion deltaRotation = Quaternion.Euler(rotationAngle * rotationSpeed * Time.deltaTime);
            _gemRigidbody.MoveRotation(_gemRigidbody.rotation * deltaRotation);
        }
    }
}
