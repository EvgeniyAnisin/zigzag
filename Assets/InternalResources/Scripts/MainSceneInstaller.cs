using DefaultNamespace;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.Scriptables;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class MainSceneInstaller : MonoInstaller
    {
        [SerializeField] private Transform parent;
        [SerializeField] private Transform popUpParent;
        [SerializeField] private TileSpawner tileSpawner;
        [SerializeField] private PlayerMovement playerMovement;
        [SerializeField] private PlayerObject playerObject;
        [SerializeField] private ScoreManager scoreManager;
        //[SerializeField] private CameraMovement cameraMovement;
        [SerializeField] private CameraFollow cameraFollow;
        public override void InstallBindings()
        {
            Container.Bind<ShopPreset>().FromScriptableObjectResource("ShopPreset").AsTransient();
            Container.BindInterfacesAndSelfTo<GemsManager>().AsSingle();
        
            Container.BindInterfacesTo<MainStateMachine>()
                .AsSingle()
                .WithArguments(MainStates.None, parent)
                .NonLazy();
            Container.BindInterfacesTo<PopUpStateMachine>()
                .AsSingle()
                .WithArguments(PopUpStates.Loading, popUpParent)
                .NonLazy();
            
            Container.BindInterfacesAndSelfTo<GemsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<TileSpawner>().FromInstance(tileSpawner).AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerMovement>().FromInstance(playerMovement).AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerObject>().FromInstance(playerObject).AsSingle();
            Container.BindInterfacesAndSelfTo<ScoreManager>().FromInstance(scoreManager).AsSingle();
            //Container.BindInterfacesAndSelfTo<CameraMovement>().FromInstance(cameraMovement).AsSingle();
            Container.BindInterfacesAndSelfTo<CameraFollow>().FromInstance(cameraFollow).AsSingle();
        }
    }
}
