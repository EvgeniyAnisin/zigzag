using InternalResources.Scripts.Player;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class CameraMovement : MonoBehaviour
    {
        private readonly Vector3 _direction = new Vector3(1f,0,1f);
        [SerializeField] private float cameraSpeed;
        [SerializeField] private float offsetSpeed;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private Vector3 offset;

        //[SerializeField] private Transform player;
        //[SerializeField] private Vector3 offset;
    
        [Inject] private PlayerObject _playerObject;

        private Vector3 _cameraPosition;

        private Vector3 _smoothPosition;
        //private bool _isCoroutineActive;

        public void ResetCameraPosition()
        {
            transform.position = _cameraPosition;
        }

        private void Start()
        {
            _cameraPosition = transform.position;
        }

        private void LateUpdate()
        {
            if (_playerObject.CanPlay)
            {
                transform.position += _direction * cameraSpeed * Time.deltaTime;
                //transform.LookAt(playerTransform, Vector3.up);

                if (playerTransform.position.x - playerTransform.position.z > 5)
                {
                    Debug.Log("X");
                    transform.position += new Vector3(1f, 0, 0f) * offsetSpeed * Time.deltaTime;
                }
                if (playerTransform.position.z - playerTransform.position.x > 5)
                {
                    Debug.Log("Z");
                    transform.position += new Vector3(0f, 0, 1f) * offsetSpeed * Time.deltaTime;
                }
                
                // if (player.position.z - player.position.x > 5)
                // {
                //     if(_isCoroutineActive) return;
                //     StartCoroutine(CameraCorrection(5));
                // }
                // Vector3 desiredPosition = player.position + offset;
                // Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, speed * Time.deltaTime);
            }
            //transform.LookAt(playerTransform, offset);
        }

        // private IEnumerator CameraCorrection(float distance)
        // {
        //     _isCoroutineActive = true;
        //     while (distance > 0)
        //     {
        //         transform.position = new Vector3(transform.position.x - 0.016f, transform.position.y, transform.position.z);
        //         distance -= 0.016f;
        //         yield return new WaitForSeconds(0.016f);
        //     }
        //
        //     _isCoroutineActive = false;
        // }
    }
}
