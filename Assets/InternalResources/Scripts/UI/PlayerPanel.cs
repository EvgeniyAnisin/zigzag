using TMPro;
using UnityEngine;

namespace UI
{
    public class PlayerPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI playerNumberText;
        [SerializeField] private TextMeshProUGUI playerNameText;
        [SerializeField] private TextMeshProUGUI playerScoreText;

        public void Init(int playerNumber, string playerName, int playerScore, bool isCurrentPlayer)
        {
            playerNumberText.text = $"{playerNumber.ToString()}.";
            playerNameText.text = isCurrentPlayer ? playerName + "(You)" : playerName;
            playerScoreText.text = playerScore.ToString();
        }
    }
}
