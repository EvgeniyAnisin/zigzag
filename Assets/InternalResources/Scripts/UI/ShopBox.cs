using InternalResources.Scripts.Player;
using InternalResources.Scripts.PopUpViewControllers.Controller;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.Scriptables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace InternalResources.Scripts.UI
{
    public class ShopBox : MonoBehaviour
    {
        [Inject] private GemsController _gemsController;
        [Inject] private PlayerObject _playerObject;
        [Inject] private UserManager _userManager;
        
        [SerializeField] private GameObject unlockedBall;
        [SerializeField] private GameObject lockedBall;
        [SerializeField] private GameObject selectedState;

        [SerializeField] private Button buyButton;
        [SerializeField] private Button selectButton;
        [SerializeField] private TextMeshProUGUI ballCostText;
        [SerializeField] private Image ballImage;

        private ShopPopUpController _shopPopUpController;
        private BoxData _boxData;
        private int _boxId;
        

        public void Init(BoxData boxData, int boxId, ShopPopUpController shopPopUpController)
        {
            _boxData = boxData;
            _boxId = boxId;
            _shopPopUpController = shopPopUpController;
            SetData();
            CheckUnlock();
            buyButton.onClick.AddListener(BuyButtonClick);
            selectButton.onClick.AddListener(SelectButtonClick);
        }

        private void SetData()
        {
            ballCostText.text = _boxData.ballCost.ToString();
            ballImage.color = _boxData.ballColor;
        }

        private void CheckUnlock()
        {
            if (_userManager.GetBoxesList().Contains(_boxId))
            {
                lockedBall.SetActive(false);
                unlockedBall.SetActive(true);
            }
            else
            {
                lockedBall.SetActive(true);
                unlockedBall.SetActive(false);
            }
        }

        public void SetSelectedState(int selectedBoxId)
        {
            selectedState.SetActive(_boxId == selectedBoxId);
        }

        private void BuyButtonClick()
        {
            if (!_gemsController.ReduceGems(_boxData.ballCost)) return;
            _userManager.AddBox(_boxId);
            CheckUnlock();
            _userManager.SaveUserSaves();
        }

        private void SelectButtonClick()
        {
            _userManager.SetSelectedBox(_boxId);
            _shopPopUpController.SetSelectedBox();
            _playerObject.SetBallMaterial(_boxId);
            _userManager.SaveUserSaves();
        }
    }
}
