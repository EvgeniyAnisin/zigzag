using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace UI
{
    public class TextVisibility : MonoBehaviour
    {
        private Color _textColor;
        private TextMeshProUGUI _text;
        private void Start()
        {
            _text = gameObject.GetComponent<TextMeshProUGUI>();
            _textColor = _text.color;
        }
    }
}
