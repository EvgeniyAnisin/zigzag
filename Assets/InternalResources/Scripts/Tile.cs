using System.Collections;
using DG.Tweening;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.Scriptables;
using UnityEngine;

namespace InternalResources.Scripts
{
    public class Tile : MonoBehaviour
    {
        [SerializeField] private Transform connectionPoint;
        [SerializeField] private Transform endPlatformConnectionPoint;
        [SerializeField] private Transform gemsSpawnPoints;
        private LevelPreset _levelPreset;
        private Rigidbody _thisRigidbody;
        private float _tileYPos;
        private Coroutine _fall;
        protected Color _defaultColor;
        protected Renderer _renderer;

        public Transform ConnectionPoint => connectionPoint;
        public Transform EndPlatformConnectionPoint => endPlatformConnectionPoint;
        public Transform GemsSpawnPoints => gemsSpawnPoints;

        private void Awake()
        {
            _renderer = transform.GetChild(0).GetComponent<Renderer>();
            _defaultColor = _renderer.material.color;
            _thisRigidbody = GetComponent<Rigidbody>();
        }

        public void Init(LevelPreset levelPreset)
        {
            _levelPreset = levelPreset;
        }

        public void SetYPosition()
        {
            _tileYPos = gameObject.transform.position.y;
        }

        public virtual void ReturnToStartPosition()
        {
            MoveTile();
        }

        public void ChangeColor(int currentLevel, float colorValue)
        {
            ColorChange(currentLevel, colorValue);
        }

        private void ColorChange(int currentLevel, float colorValue)
        {
            var newColor = colorValue == 0 ? _defaultColor : _levelPreset.LevelData[currentLevel].PlatformsColor.Evaluate(colorValue);
            _renderer.material.DOColor(newColor, 1);
        }
    
        private void OnTriggerEnter(Collider other)
        {
            if(!other.GetComponent<PlayerMovement>()) return;
            _fall = StartCoroutine(Fall());
        }

        private void MoveTile()
        {
            if(_fall != null) StopCoroutine(_fall);
            _thisRigidbody.isKinematic = true;
            Vector3 startPosition = transform.position;
            Vector3 endPosition = new Vector3(startPosition.x, _tileYPos, startPosition.z);
            transform.DOMove(endPosition, 1);
        }

        private IEnumerator Fall()
        {
            yield return new WaitForSeconds(2);
            _thisRigidbody.isKinematic = false;
            yield return new WaitForSeconds(3);
            _thisRigidbody.isKinematic = true;
        }

        private void OnDisable()
        {
            _thisRigidbody.isKinematic = true;
            _renderer.material.SetColor("_Color", _defaultColor);
        }
    }
}
