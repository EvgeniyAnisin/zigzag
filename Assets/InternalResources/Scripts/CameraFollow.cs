using System;
using InternalResources.Scripts.Player;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class CameraFollow : MonoBehaviour
    {
        [Inject] private PlayerObject _playerObject;
        
        [SerializeField] private Transform playerTransform;

        [SerializeField] private float speed;
        [SerializeField] private Vector3 offset;

        private void Start()
        {
            ResetCameraPosition();
        }

        public void ResetCameraPosition()
        {
            transform.position = playerTransform.position + offset;
        }

        private void LateUpdate()
        {
            if (_playerObject.CanPlay)
            {
                Vector3 desiredPosition = playerTransform.position + offset;
                Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, speed * Time.deltaTime);
                transform.position = smoothPosition;
            }
        }
    }
}