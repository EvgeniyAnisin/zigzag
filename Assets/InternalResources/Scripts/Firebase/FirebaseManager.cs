using System;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using InternalResources.Scripts.Save;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.Firebase
{
    public class FirebaseManager :  IInitializable
    {
        public Action<bool> LoginScreenState;
        
        public Action<string> DisplayRegistrationErrorMessage;
        public Action<string> DisplayLoginErrorMessage;
        
        public Action<User> OnUserReceived;
        
        private FirebaseAuth _auth;
        private FirebaseUser _user;
        private DatabaseReference _databaseReference;

        private DatabaseReference _playerDataPath;
        private DatabaseReference _playerSavesPath;
        private DatabaseReference _playerNamePath;
        private DatabaseReference _playerNoAdsPath;
        private Query _playerScorePath;

        public bool IsUserLoggedIn { get; private set; }

        public void Initialize()
        {
            _databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
            _auth = FirebaseAuth.DefaultInstance;
            _auth.StateChanged += AuthStateChanged;
            AuthStateChanged(this, null);
        }

        public void CreateNewUser(string email, string password, string playerName)
        {
            Debug.Log(email + " " + password);
            _auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
            {
                if (task.IsCanceled) {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    if(task.Exception?.Flatten().InnerException is FirebaseException firebaseException) 
                        DisplayFirebaseException((AuthError) firebaseException.ErrorCode, MenuName.Register);
                    return;
                }

                // Firebase user has been created.
                FirebaseUser newUser = task.Result;
                Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
                _user = newUser;
                SetNewPlayerData(playerName);
                SetNewPlayerSaves();
            });
        }

        public void LoginUser(string email, string password)
        {
            _auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    if(task.Exception?.Flatten().InnerException is FirebaseException firebaseException) 
                        DisplayFirebaseException((AuthError) firebaseException.ErrorCode, MenuName.Login);
                    return;
                }

                FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
                _user = newUser;
            });
        }

        public void LogoutUser()
        {
            _auth.SignOut();
        }

        public void SavePlayerData(PlayerData data)
        {
            string playerJson = JsonUtility.ToJson(data);
            _playerDataPath.SetRawJsonValueAsync(playerJson);
        }

        public void SavePlayerSaves(PlayerSaves saves)
        {
            string playerSavesJson = JsonUtility.ToJson(saves);
            _playerSavesPath.SetRawJsonValueAsync(playerSavesJson);
        }

        public void SetPlayerName(string playerName)
        {
            _playerNamePath.SetValueAsync(playerName);
        }

        public void SetNoAdsState(bool state)
        {
            _playerNoAdsPath.SetValueAsync(state);
        }

        private void DisplayFirebaseException(AuthError errorCode, MenuName menuName)
        {
            string message = errorCode.ToString();
            Debug.Log(message);
            switch (menuName)
            {
                case MenuName.Register:
                    DisplayRegistrationErrorMessage?.Invoke(message);
                    break;
                case MenuName.Login:
                    DisplayLoginErrorMessage?.Invoke(message);
                    break;
            }
        }
        
        private void SetNewPlayerData(string playerName)
        {
            PlayerData playerData = new PlayerData(0,0,0);
            string playerDataJson = JsonUtility.ToJson(playerData);
            _playerDataPath.SetRawJsonValueAsync(playerDataJson);
            SetPlayerName(playerName);
            SetNoAdsState(false);
        }

        private void SetNewPlayerSaves()
        {
            PlayerSaves playerSaves = new PlayerSaves();
            string playerSavesJson = JsonUtility.ToJson(playerSaves);
            _playerSavesPath.SetRawJsonValueAsync(playerSavesJson);
        }

        public async Task<User> GetPlayerData()
        {
            var data = await _playerDataPath.GetValueAsync();
            var saves = await _playerSavesPath.GetValueAsync();
            var playerName = await _playerNamePath.GetValueAsync();
            var playerNoAds = await _playerNoAdsPath.GetValueAsync();
            Debug.Log(playerNoAds.Value == null);
            var playerNoAdsState = playerNoAds.Value != null && bool.Parse(playerNoAds.Value.ToString());
            PlayerData playerData = JsonUtility.FromJson<PlayerData>(data.GetRawJsonValue());
            PlayerSaves playerSaves = JsonUtility.FromJson<PlayerSaves>(saves.GetRawJsonValue());
            if (playerSaves == null) playerSaves = new PlayerSaves();
            User user = new User(playerName.Value.ToString(), playerNoAdsState, playerData, playerSaves);
            OnUserReceived?.Invoke(user);
            return user;
        }

        public async Task<DataSnapshot> GetLeaders()
        {
            var leaders = await _playerScorePath.LimitToLast(5)
                .GetValueAsync();
            return leaders;
        }
        
        public FirebaseUser GetUser()
        {
            return _user;
        }

        private void CreatePlayerPath()
        {
            _playerDataPath = _databaseReference.Child("users").Child(_user.UserId).Child("playerData");
            _playerSavesPath = _databaseReference.Child("users").Child(_user.UserId).Child("playerSaves");
            _playerNamePath = _databaseReference.Child("users").Child(_user.UserId).Child("playerName");
            _playerNoAdsPath = _databaseReference.Child("users").Child(_user.UserId).Child("playerNoAds");
            _playerScorePath = _databaseReference.Child("users").OrderByChild("playerData/playerScore");
        }

        private void AuthStateChanged(object sender, EventArgs eventArgs) {
            if (_auth.CurrentUser != _user) {
                bool signedIn = _user != _auth.CurrentUser && _auth.CurrentUser != null;
                if (!signedIn && _user != null) {
                    Debug.Log("Signed out " + _user.UserId);
                    IsUserLoggedIn = false;
                    LoginScreenState?.Invoke(false);
                }
                _user = _auth.CurrentUser;
                if (signedIn) {
                    Debug.Log("Signed in " + _user.UserId);
                    CreatePlayerPath();
                    IsUserLoggedIn = true;
                    LoginScreenState?.Invoke(true);
                }
            }
        }
    }

    public enum MenuName
    {
        Register,
        Login
    }
}
