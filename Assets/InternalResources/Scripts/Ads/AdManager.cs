using System;
using GoogleMobileAds.Api;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.Ads
{
    public class AdManager : IInitializable
    {
        private string _interstitialUnitId = "ca-app-pub-3940256099942544/1033173712";

        private InterstitialAd _interstitialAd;
        private AdRequest _adRequest;
        
        public bool IsNoAdsActive { get; set; }
        

        public void ShowAd()
        {
            if (_interstitialAd.IsLoaded())
            {
                _interstitialAd.Show();
            }
            else LoadAd(_adRequest);
        }

        public void Initialize()
        {
            
            MobileAds.Initialize(initStatus =>
            {
                _interstitialAd = new InterstitialAd(_interstitialUnitId);
                _adRequest = new AdRequest.Builder().Build();
                LoadAd(_adRequest);
                _interstitialAd.OnAdClosed += HandleOnAdClosed;
                Debug.Log("Initialize");
            });
    
        }

        private void LoadAd(AdRequest adRequest)
        {
            _interstitialAd.LoadAd(adRequest);
            Debug.Log(_interstitialAd == null);
        }

        private void HandleOnAdClosed(object sender, EventArgs args)
        {
            LoadAd(_adRequest);
        }

        private void CheckNoAds()
        {
            IsNoAdsActive = Convert.ToBoolean(PlayerPrefs.GetInt("NoAds",0));
        }
    }
}
