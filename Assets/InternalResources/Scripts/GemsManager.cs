using InternalResources.Scripts.Save;
using Zenject;

namespace InternalResources.Scripts
{
    public class GemsManager
    {
        [Inject] private UserManager _userManager;
        
        private int _collectedGemsCount;

        public int GemsCount() => _userManager.GetGems();

        public void AddGem(int count = 1)
        {
            _userManager.AddGems(count);
            SaveGems();
        }

        public bool ReduceGems(int amount)
        {
            if (amount > GemsCount()) return false;
            _userManager.ReduceGems(amount);
            SaveGems();
            return true;
        }

        public void SetGems(int amount)
        {
            _userManager.SetGems(amount);
            SaveGems();
        }
    
        public void CollectGem()
        {
            _collectedGemsCount++;
        }

        public void ResetCollectedGems()
        {
            _collectedGemsCount = 0;
        }

        public int GetCollectedGemsCount()
        {
            return _collectedGemsCount;
        }

        private void SaveGems()
        {
            _userManager.SaveUserData();
        }
    }
}
