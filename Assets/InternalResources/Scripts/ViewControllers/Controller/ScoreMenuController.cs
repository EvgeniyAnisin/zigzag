using Core.Scripts.StateMachine;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.ViewControllers.Controller
{
    public class ScoreMenuController : MonoBehaviour
    {
        [Inject] private IStateMachine<MainStates> _stateMachine;
        [Inject] private PlayerMovement _playerMovement;
        [Inject] private TileSpawner _tileSpawner;
        
        public void NextLevelButtonClicked()
        {
            _stateMachine.Fire(MainStates.MainMenu);
            _playerMovement.ResetBallPosition();
            _tileSpawner.RestartLevel(true);
            _tileSpawner.ResetTiles();
        }
        
        public void RetryButtonClick()
        {
            _stateMachine.Fire(MainStates.MainMenu);
            _tileSpawner.ReturnTilesToPosition();
            _playerMovement.ResetBallPosition();
            _tileSpawner.ResetTiles();
        }
    }
}
