using System;
using Core.Scripts.StateMachine;
using Firebase;
using InternalResources.Scripts.Firebase;
using InternalResources.Scripts.Player;
using InternalResources.Scripts.PopUpViewControllers;
using InternalResources.Scripts.PopUpViewControllers.View;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.ViewControllers.Controller
{
    public class MainMenuController : MonoBehaviour
    {
        [Inject] private IStateMachine<MainStates> _stateMachine;
        [Inject] private IStateMachine<PopUpStates> _popUpStateMachine;
        [Inject] private PlayerObject _playerObject;

        private bool _isSoundMuted;
        
        
        public Action SoundAction;

        public bool IsSoundMuted => _isSoundMuted;

        public void CheckSound()
        {
            _isSoundMuted = PlayerPrefs.GetInt("SoundMuted") != 0;
        }

        public void PlayButtonClick()
        {
            _stateMachine.Fire(MainStates.ActionMenu);
            _playerObject.SetPlayState(true);
        }

        public void NoAdsButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.NoAds);
        }

        public void LeaderboardButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.Leaderboard);
        }

        public void SoundButtonClick()
        {
            _isSoundMuted = !_isSoundMuted;
            SoundAction?.Invoke();
            PlayerPrefs.SetInt("SoundMuted", Convert.ToInt32(_isSoundMuted));
        }

        public void SettingsButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.Settings);
        }

        public void ShopButtonClick()
        {
            _popUpStateMachine.Fire(PopUpStates.Shop);
        }
        
    }
}
