using Core.Scripts.StateMachine;

namespace InternalResources.Scripts.ViewControllers.View
{
    public class ActionMenuView : ViewController<MainStates>
    {
        public override MainStates ViewState => MainStates.ActionMenu;
    }
}
