using Core.Scripts.StateMachine;
using InternalResources.Scripts.ViewControllers.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace InternalResources.Scripts.ViewControllers.View
{
    public class ScoreMenuView : ViewController<MainStates>
    {
        public override MainStates ViewState => MainStates.ScoreMenu;
        
        [Header("Controller")]
        [SerializeField] private ScoreMenuController scoreMenuController;
        
        [SerializeField] private Button nextLevel;
        [SerializeField] private Button retry;
        
        public override void Initialize()
        {
            nextLevel.onClick.AddListener(scoreMenuController.NextLevelButtonClicked);
            retry.onClick.AddListener(scoreMenuController.RetryButtonClick);
        }
    }
}
