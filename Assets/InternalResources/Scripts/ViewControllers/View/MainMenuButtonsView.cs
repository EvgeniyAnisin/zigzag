using Core.Scripts.StateMachine;
using InternalResources.Scripts.ViewControllers.Controller;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace InternalResources.Scripts.ViewControllers.View
{
    public class MainMenuButtonsView : ViewController<MainStates>
    {
        public override MainStates ViewState => MainStates.MainMenu;

        [Header("Controller")] 
        [SerializeField] private MainMenuController mainMenuController;
        
        [Header("View")]
        [SerializeField] private MainMenuTextView mainMenuTextView;
        
        [SerializeField] private Button playButton;
        [SerializeField] private Button noAdsButton;
        [SerializeField] private Button leaderboardButton;
        [SerializeField] private Button soundButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button shopButton;
        [SerializeField] private AudioMixer soundMixer;
        
        [Header("Sound sprites")] 
        [SerializeField] private Sprite soundOnSprite;
        [SerializeField] private Sprite soundOffSprite;
        
        private Image _soundButtonImage;

        public override void Initialize()
        {
            _soundButtonImage = soundButton.GetComponent<Image>();
            InitButtons();
            mainMenuTextView.Init();
            mainMenuController.CheckSound();
            SetSound();
        }

        public void SetNoAdsButton(bool state)
        {
            Debug.Log(!state);
            noAdsButton.gameObject.SetActive(!state);
        }
        
        private void InitButtons()
        {
            leaderboardButton.onClick.AddListener(mainMenuController.LeaderboardButtonClick);
            playButton.onClick.AddListener(mainMenuController.PlayButtonClick);
            noAdsButton.onClick.AddListener(mainMenuController.NoAdsButtonClick);
            soundButton.onClick.AddListener(mainMenuController.SoundButtonClick);
            settingsButton.onClick.AddListener(mainMenuController.SettingsButtonClick);
            shopButton.onClick.AddListener(mainMenuController.ShopButtonClick);
            mainMenuController.SoundAction += SetSound;
        }

        private void SetSound()
        {
            _soundButtonImage.sprite = mainMenuController.IsSoundMuted ? soundOffSprite : soundOnSprite;
            var soundValue = mainMenuController.IsSoundMuted ? -80 : 0;
            soundMixer.SetFloat("SoundVolume", soundValue);
        }

        private void OnDestroy()
        {
            mainMenuController.SoundAction -= SetSound;
        }
    }
    
    public enum MainStates
    {
        None,
        MainMenu,
        ActionMenu,
        ScoreMenu
    }
}
