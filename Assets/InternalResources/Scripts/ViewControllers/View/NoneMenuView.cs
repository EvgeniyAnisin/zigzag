using Core.Scripts.StateMachine;
using UnityEngine;

namespace InternalResources.Scripts.ViewControllers.View
{
    public class NoneMenuView : ViewController<MainStates>
    {
        public override MainStates ViewState => MainStates.None;
    }
}
