using System;
using Core.Scripts.CoreScripts.Observer;
using InternalResources.Scripts.Save;
using TMPro;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.ViewControllers.View
{
    public class MainMenuTextView : MonoBehaviour, IObservableNotifier<GemsManager>
    {
        [Inject] private GemsManager _gemsManager;
        [Inject] private UserManager _userManager;
        [Inject] private IDataObservable<GemsManager> _observable;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private TextMeshProUGUI gemsText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI totalsScore;
        [SerializeField] private TextMeshProUGUI levelScore;
        
        private IDisposable _disposable;

        private void OnEnable()
        {
            SetText();
        }

        public void Init()
        {
            _disposable = _observable.Subscribe(this);
        }

        public void Notify(GemsManager data)
        {
            gemsText.text = data.GemsCount().ToString();
        }
        
        private void SetText()
        {
            levelText.text = $"LEVEL {_userManager.GetLevel() + 1}";
            gemsText.text = _gemsManager.GemsCount().ToString();
            totalsScore.text = _userManager.GetScore().ToString();
            levelScore.text = scoreManager.LevelScore.ToString();
        }
        
        private void OnDestroy()
        {
            _disposable?.Dispose();
        }
    }
}
