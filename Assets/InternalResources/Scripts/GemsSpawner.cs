using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace InternalResources.Scripts
{
    public class GemsSpawner : MonoBehaviour
    {
        [Inject] private DiContainer _container;
        [SerializeField] private GameObject gemPrefab;
        [SerializeField] private Transform gemsPool;
        [SerializeField] private Transform particlePool;
        [SerializeField] private GameObject particlePrefab;
        [SerializeField] private Transform gemsInGamePool;

        public Transform GemsInGamePool => gemsInGamePool;
    
        private int _gemsRepeatCounter = 0;

        private void Start()
        {
            CreateGems(20);
        }

        public void HideGems()
        {
            for (int i = gemsInGamePool.childCount - 1; i >= 0; i--)
            {
                var gem = gemsInGamePool.GetChild(i);
                gem.gameObject.SetActive(false);
                gem.SetParent(gemsPool);
            }
        }

        public void RefreshGems()
        {
            for (int i = 0; i < gemsInGamePool.childCount; i++)
            {
                var gem = gemsInGamePool.GetChild(i).gameObject;
                if(!gem.activeSelf) gem.SetActive(true);
            }
        }

        private void CreateGems(int count)
        {
            for (int j = 0; j < count; j++)
            {
                var gem = Instantiate(gemPrefab, gemsPool.transform.position, gemPrefab.transform.rotation, gemsPool);
                var gemCollect = gem.GetComponent<GemCollect>();
                _container.Inject(gemCollect);
                gemCollect.Init(gemsPool, particlePool, particlePrefab);
                gem.name = gemPrefab.name;
                gem.SetActive(false);
            }
        }

        private GameObject CreateGem()
        {
            var gem = Instantiate(gemPrefab, gemsPool.transform.position, gemPrefab.transform.rotation, gemsPool);
            var gemCollect = gem.GetComponent<GemCollect>();
            _container.Inject(gemCollect);
            gemCollect.Init(gemsPool, particlePool, particlePrefab);
            gem.name = gemPrefab.name;
            return gem;
        }

        public void SpawnGems(Transform gemsSpawnPoints)
        {
            Random.InitState(Random.Range(0,999999999));
            for (int i = 0; i < gemsSpawnPoints.childCount; i++)
            {
                var gemSpawnChance = Random.Range(0, 11);
                if (_gemsRepeatCounter == 1)
                {
                    _gemsRepeatCounter = 0;
                    continue;
                }

                if (gemSpawnChance > 3) continue;
                var gem =  GetGem();
                gem.transform.position = gemsSpawnPoints.GetChild(i).transform.position;
                gem.transform.SetParent(gemsInGamePool);
                _gemsRepeatCounter++;
            }
        }

        private GameObject GetGem()
        {
            for (int i = 0; i < gemsPool.childCount; i++)
            {
                var gem = gemsPool.GetChild(i);
                if (!gem.gameObject.activeSelf)
                {
                    GameObject gemGameObject;
                    (gemGameObject = gem.gameObject).SetActive(true);
                    return gemGameObject;
                }
            }

            return CreateGem();
        }
    }
}
