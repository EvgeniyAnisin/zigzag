using InternalResources.Scripts.Player;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts
{
    public class GemCollect : MonoBehaviour
    {
        [Inject] private GemsController _gemsController;
        [Inject] private GemsManager _gemsManager;
        private Transform _gemsPool;
        private Transform _particlePool;
        private GameObject _particlePrefab;

        public void Init(Transform gemsPool, Transform particlePool, GameObject particlePrefab)
        {
            _gemsPool = gemsPool;
            _particlePool = particlePool;
            _particlePrefab = particlePrefab;
        }
    
        private void OnTriggerEnter(Collider other)
        {
            if (!(other.GetComponent<PlayerMovement>())) return;
            //_gemsController.AddGem();
            _gemsManager.CollectGem();
            //gameObject.transform.SetParent(_gemsPool);
            gameObject.SetActive(false);
            GetParticle().Play();
        }

        private ParticleSystem GetParticle()
        {
            for (int i = 0; i < _particlePool.childCount; i++)
            {
                var particle = _particlePool.GetChild(i).gameObject;
                if (!particle.activeSelf)
                {
                    particle.transform.position = transform.position;
                    particle.SetActive(true);
                    return particle.GetComponent<ParticleSystem>();
                }
            }

            return Instantiate(_particlePrefab, transform.position, Quaternion.identity, _particlePool)
                .GetComponent<ParticleSystem>();
        }
    }
}
