using InternalResources.Scripts.Player;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace InternalResources.Scripts
{
    public class PlayerInput : MonoBehaviour, IPointerDownHandler
    {
        [Inject] private PlayerMovement _playerMovement;
        private Vector3 _direction;

        public void OnPointerDown(PointerEventData eventData)
        {
            _direction = !_playerMovement.IsDirectionChanged ? new Vector3(0, 0, 1) : new Vector3(1, 0, 0);
            _playerMovement.SetDirection(_direction);
        }
    }
}
