using System;
using InternalResources.Scripts.Scriptables;
using UnityEngine;
using UnityEngine.Purchasing;
using Zenject;

namespace InternalResources.Scripts.InAppPurchasing
{
    public class InAppManager : IStoreListener, IInitializable
    {
        private IStoreController _controller;
        private IExtensionProvider _extensions;
        [Inject] private Products _iapProducts;
        
        public event Action<ProductData> PurchaseCompleted;
        public event Action<ProductData, PurchaseFailureReason> PurchaseFailed;
        
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("Purchase OnInitialized: PASS");
            _controller = controller;
            _extensions = extensions;
        }

        public void Initialize()
        {
            InitializePurchasing();
        }

        public void Purchase(string id)
        {
            ProductData pd = GetIAPProductById(id);

            if (pd != null && pd.id != null)
            {
                PurchaseWithId(pd.id);
            }
            else
            {
                PurchaseFailed?.Invoke(pd, PurchaseFailureReason.ProductUnavailable);
            }
        }

        private bool IsInitialized()
        {
            return _controller != null && _extensions != null;
        }
        
        void PurchaseWithId(string productId)
        {
            ProductData pd = GetIAPProductById(productId);
            if (IsInitialized())
            {
                Product product = _controller.products.WithID(productId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.Log($"Purchasing product asynchronously: '{product.definition.id}'");
                    _controller.InitiatePurchase(product);

                }
                else
                {
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
                }
            }
            else
            {
                  //Toast.instance.ShowMessage("No internet connection");
                PurchaseFailed(pd, PurchaseFailureReason.PurchasingUnavailable);
            }
        }
        
        public ProductData GetIAPProductById(string productId)
        {
            foreach (ProductData pd in _iapProducts.ProductData)
            {
                if (pd.id.Equals(productId))
                    return pd;
            }

            return null;
        }

        private void InitializePurchasing()
        {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (var product in _iapProducts.ProductData)
                builder.AddProduct(product.id, (ProductType)product.type);
            UnityPurchasing.Initialize(this, builder);
        }
        
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("Billing failed to initialize!");
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    Debug.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Debug.Log("No products available for purchase!");
                    break;
            }
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
        {
            ProductData product = GetIAPProductById(purchaseEvent.purchasedProduct.definition.id);
            PurchaseCompleted?.Invoke(product);
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product item, PurchaseFailureReason failureReason)
        {
            Debug.Log("Purchase failed: " + item.definition.id);
            Debug.Log(failureReason);

            ProductData product = GetIAPProductById(item.definition.id);
            PurchaseFailed?.Invoke(product, failureReason);
        }
    }
}
