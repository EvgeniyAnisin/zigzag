using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.Scriptables;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace InternalResources.Scripts
{
    public class TileSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject startPlatform;
        [SerializeField] private GameObject[] endTiles;
        [SerializeField] private GameObject[] tiles;
        [SerializeField] private Transform tilesParent;
        [SerializeField] private Transform tilesInGameParent;
        [SerializeField] private GemsSpawner gemsSpawner;
        [SerializeField] private LevelPreset levelPreset;

        [Inject] private DiContainer _container;
        [Inject] private UserManager _userManager;

        private List<Tile> _tiles = new List<Tile>();
        private List<Tile> _tilesInGame = new List<Tile>();

        public LevelPreset LevelPreset => levelPreset;
        public int LevelsCount => levelPreset.LevelData.Length;
        public Transform TilesInGameParent => tilesInGameParent;
        
        public int TilesReached => _tilesReached;

        private int _currentLevel;

        private int _tileId;
        private int _previousTileId = -1;
        private Tile _createdTile;
        private Tile _previousTile;
        
        private int _leftTileRepeatCounter = 2;
        private int _rightTileRepeatCounter = 2;
        
        private int _tilesCounter;
        private int _tilesReached;
        private int _tilesAddStep;

        private void Start()
        {
            Application.targetFrameRate = 120;
            CreateTiles(10);
            CreateEndTiles();
            SetPlatformsCount();
        }

        public void ReturnTilesToPosition()
        {
            StartCoroutine(ReturnTilesDelay());
            foreach (var tile in _tilesInGame)
            {
                tile.ChangeColor(_currentLevel, 0);
            }
            gemsSpawner.RefreshGems();
        }
        
        public void PassTile()
        {
            _tilesReached++;
            if (_tilesReached < _tilesCounter) return;
            ChangeTilesColor(_tilesReached);
            _tilesCounter += _tilesAddStep;
        }

        public void ResetTiles()
        {
            ResetReachedTiles();
            SetPlatformsCount();
        }

        private void ResetReachedTiles()
        {
            _tilesReached = 0;
            _tilesCounter = _tilesAddStep;
        }
        
        private void SetPlatformsCount()
        {
            _tilesAddStep = LevelPreset.LevelData[_currentLevel].PlatformsColorChangeCount;
            _tilesCounter = _tilesAddStep;
        }

        public void RestartLevel(bool spawnNewLevel)
        {
            StartCoroutine(Restart(spawnNewLevel));
        }

        private void ReturnTilesToPool()
        {
            for (int i = _tilesInGame.Count - 1; i >=0; i--)
            {
                var tile = _tilesInGame[i];
                tile.gameObject.SetActive(false);
                tile.transform.SetParent(tilesParent);
                _tiles.Add(tile);
                _tilesInGame.Remove(tile);
            }
        }

        private void ChangeTilesColor(int platformsReached)
        {
            foreach (var tile in _tilesInGame)
            {
                tile.ChangeColor(_currentLevel, (float)platformsReached/levelPreset.LevelData[_currentLevel].PlatformsCount);
            }
        }

        private IEnumerator Restart(bool spawnNewLevel)
        {
            gemsSpawner.HideGems();
            ReturnTilesToPool();
            yield return new WaitForSeconds(0.3f);
            if(spawnNewLevel) SpawnTiles();
        }

        private IEnumerator ReturnTilesDelay()
        {
            foreach (var tile in _tilesInGame)
            {
                tile.ReturnToStartPosition();
                yield return new WaitForSeconds(0.1f);
            }
        }

        public void SpawnTiles()
        {
            _currentLevel = _userManager.GetLevel();
            var previousTileConnectionPoint = startPlatform.transform.GetChild(0).transform.position;
            Random.InitState(Random.Range(0, 999999999));
            var levelData = levelPreset.LevelData[_currentLevel];
            for (int i = 0; i < levelData.PlatformsCount; i++)
            {
                _tileId = Random.Range(0, 2);
                _tileId = _tileId == 0 && _rightTileRepeatCounter > 0 ? 0 : 1;
                _tileId = _tileId == 1 && _leftTileRepeatCounter > 0 ? 1 : 0;
                _createdTile = GetTile(tiles[_tileId].name, _tileId);
                var localScale = _createdTile.transform.localScale;
                localScale = _tileId == 1
                    ? new Vector3(
                        Random.Range(levelData.LeftTileScaleXRange[0], levelData.LeftTileScaleXRange[1]), localScale.y,
                        _previousTileId == _tileId
                            ? _previousTile.transform.localScale.z
                            : Random.Range(levelData.LeftTileScaleZRange[0], levelData.LeftTileScaleZRange[1])) // left
                    : new Vector3(_previousTileId == _tileId
                            ? _previousTile.transform.localScale.x
                            : Random.Range(levelData.RightTileScaleXRange[0], levelData.RightTileScaleXRange[1]), localScale.y,
                        Random.Range(levelData.RightTileScaleZRange[0], levelData.RightTileScaleZRange[1])); // right
                _createdTile.transform.localScale = localScale;
                _createdTile.transform.position = previousTileConnectionPoint;
                _createdTile.SetYPosition();
                if (i % 2 == 0) gemsSpawner.SpawnGems(_createdTile.GemsSpawnPoints);
                previousTileConnectionPoint = _createdTile.ConnectionPoint.position;

                if (_tileId == 0)
                {
                    if(_rightTileRepeatCounter <= 2) _rightTileRepeatCounter--;
                    if (_leftTileRepeatCounter >= 0 && _leftTileRepeatCounter < 2) _leftTileRepeatCounter++;
                }
                else
                {
                    if(_leftTileRepeatCounter <= 2) _leftTileRepeatCounter--;
                    if (_rightTileRepeatCounter >= 0 && _rightTileRepeatCounter < 2) _rightTileRepeatCounter++;
                }

                _previousTileId = _tileId;
                _previousTile = _createdTile;
                _tilesInGame.Add(_createdTile);
                _tiles.Remove(_createdTile);
                _createdTile.transform.SetParent(tilesInGameParent);
            }
        
            previousTileConnectionPoint = _createdTile.EndPlatformConnectionPoint.position;
            var endTile = GetTile(endTiles[_tileId].name, _tileId);
            endTile.transform.position = previousTileConnectionPoint;
            _tilesInGame.Add(endTile);
            _tiles.Remove(endTile);
            endTile.transform.SetParent(tilesInGameParent);
        }

        private void CreateTiles(int count)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    var tile = Instantiate(tiles[i], tilesParent.transform.position, Quaternion.identity, tilesParent);
                    tile.name = tiles[i].name;
                    tile.SetActive(false);
                    var tileObject = tile.GetComponent<Tile>();
                    tileObject.Init(levelPreset);
                    _tiles.Add(tileObject);
                }
            }
        }

        private Tile CreateTile(int tileId)
        {
            var tile = Instantiate(tiles[tileId], tilesParent.transform.position, Quaternion.identity, tilesParent);
            tile.name = tiles[tileId].name;
            var tileObject = tile.GetComponent<Tile>();
            tileObject.Init(levelPreset);
            _tiles.Add(tileObject);
            return tileObject;
        }

        private void CreateEndTiles()
        {
            foreach (var tile in endTiles)
            {
                var endTile = Instantiate(tile, tilesParent.transform.position, Quaternion.identity, tilesParent);
                var levelComplete = endTile.GetComponentInChildren<LevelComplete>();
                _container.Inject(levelComplete);
                endTile.name = tile.name;
                endTile.SetActive(false);
                var endTileObject = endTile.GetComponent<Tile>();
                endTileObject.Init(levelPreset);
                _tiles.Add(endTileObject);
            }
        }

        private Tile GetTile(string tileName, int tileId)
        {
            foreach (var tile in _tiles.Where(tile => tile.name == tileName && !tile.gameObject.activeSelf))
            {
                tile.gameObject.SetActive(true);
                return tile;
            }

            return CreateTile(tileId);
        }
    }
}
