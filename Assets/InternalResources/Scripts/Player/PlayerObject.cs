using System;
using Core.Scripts.StateMachine;
using InternalResources.Scripts.Ads;
using InternalResources.Scripts.Save;
using InternalResources.Scripts.Scriptables;
using InternalResources.Scripts.ViewControllers.View;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.Player
{
    public class PlayerObject : MonoBehaviour
    {
        [Inject] private IStateMachine<MainStates> _stateMachine;
        [Inject] private ScoreManager _scoreManager;
        [Inject] private AdManager _adManager;
        [Inject] private UserManager _userManager;
        [Inject] private ShopPreset _shopPreset;

        public bool IsDead { get; private set; }

        public bool CanPlay { get; private set; }

        private int _playerDeath;

        public void SetBallMaterial()
        {
            var selectedBoxId = _userManager.GetSelectedBox();
            GetComponent<Renderer>().material = _shopPreset.BoxData[selectedBoxId].ballMaterial;
        }

        public void SetPlayState(bool state)
        {
            CanPlay = state;
        }

        public void SetDeadState(bool state)
        {
            IsDead = state;
        }

        public void SetBallMaterial(int ballId)
        {
            GetComponent<Renderer>().material = _shopPreset.BoxData[ballId].ballMaterial;
        }
        
        public void Death()
        {
            IsDead = !IsDead;
            _scoreManager.CalculateWinScore(ScoreManager.State.Lose);
            _stateMachine.Fire(MainStates.ScoreMenu);
            SetPlayState(false);
            Debug.Log("LOSE");
            _userManager.SaveUserData();
            if(_adManager.IsNoAdsActive) return;
            _playerDeath++;
            PlayerPrefs.SetInt("PlayerDeath",_playerDeath);
            if (_playerDeath == 2)
            {
                _adManager.ShowAd();
                _playerDeath = 0;
                PlayerPrefs.SetInt("PlayerDeath",_playerDeath);
            }
        }
    }
}
