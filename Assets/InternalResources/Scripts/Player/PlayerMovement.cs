using System;
using Core.Scripts.StateMachine;
using Firebase;
using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.Player
{
    [RequireComponent(typeof(PlayerObject))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float speed;
        [Inject] private PlayerObject _playerObject;
        [Inject] private CameraFollow _cameraFollow;

        private Vector3 _direction;
        private bool _isDirectionChanged;
        private Vector3 _ballPosition;
        private Rigidbody _ballRigidbody;
        
        public bool IsDirectionChanged => _isDirectionChanged;

        private void Start()
        {
            QualitySettings.vSyncCount = 0;
            _ballRigidbody = GetComponent<Rigidbody>();
            _ballPosition = transform.position;
            _direction = new Vector3(1,0,0);
        }

        public void SetDirection(Vector3 direction)
        {
            _direction = direction;
            _isDirectionChanged = !_isDirectionChanged;
        }
        
        private void FixedUpdate()
        {
            if(!_playerObject.CanPlay) return;
            _ballRigidbody.MovePosition(transform.position + _direction * speed * Time.deltaTime);
            //transform.position += _direction * speed * Time.deltaTime;
        }

        public void StopBall()
        {
            _direction = Vector3.zero;
        }

        public void ResetBallPosition()
        {
            _playerObject.SetPlayState(false);
            _playerObject.SetDeadState(false);
            _direction = new Vector3(1,0,0);
            _isDirectionChanged = false;
            gameObject.transform.position = _ballPosition;
            _cameraFollow.ResetCameraPosition();
        }
    }
}
