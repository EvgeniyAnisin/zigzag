using UnityEngine;
using Zenject;

namespace InternalResources.Scripts.Player
{
    public class PlayerCollisions : MonoBehaviour
    {
        [Inject] private TileSpawner _tileSpawner;
        [Inject] private PlayerObject _playerObject;
        private void OnCollisionExit(Collision other)
        {
            _tileSpawner.PassTile();
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if(_playerObject.IsDead) return;
            if(other.gameObject.CompareTag("LoseArea")) _playerObject.Death();
        }
    }
}
